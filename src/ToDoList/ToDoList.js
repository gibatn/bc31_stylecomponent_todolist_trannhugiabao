import React, { Component } from "react";
import { ThemeProvider } from "styled-components";
import { Container } from "../ComponentToDoList/Container";
import { ToDoListDarkTheme } from "../Theme/ToDoListDarkThem";
import { ToDoListLightTheme } from "../Theme/ToDoListLightTheme";
import { ToDoListPrimaryTheme } from "../Theme/ToDoListPrimaryTheme";
import { Dropdown } from "../ComponentToDoList/Dropdown";
import { Label, TextField, Input } from "../ComponentToDoList/TextField";
import { Table, Tr, Td, Th, Thead, Tbody } from "../ComponentToDoList/Table";
import {
  addTaskAction,
  changeTheme,
  doneTask,
  deleteTask,
  editTask,
  updateTask,
} from "../redux/actions/ToDoListAction";
import {
  Heading1,
  Heading2,
  Heading3,
  Heading4,
  Heading5,
} from "../ComponentToDoList/Heading";
import { Button } from "../ComponentToDoList/Button";
import { connect } from "react-redux";
import { arrTheme } from "../Theme/ThemeManager";
class ToDoList extends Component {
  state = {
    taskName: "",
    disabled: true,
  };
  renderTheme = () => {
    return arrTheme.map((item) => {
      return <option value={item.id}>{item.name}</option>;
    });
  };
  //  life cycle bảng 16 nhận vào props mới được thực thi trước render
  // componentWillReceiveProps(newProps) {
  //   this.setState({ taskName: newProps.taskEdit.taskName });
  // }

  // static getDerivedStateFromProps(newProps, currentState) {
  //   //  newProps: props mới,props cũ là this.props ( không truy xuất được con trỏ this trong static(tĩnh))
  //   //  currentState ứng với state hiện tại = this.state
  //   // trả về state mới
  //   let newState = { ...currentState, taskName: newProps.taskEdit.taskName };
  //   return newState;

  //   //  trả về state giữ nguyễn
  //   // return null;
  // }
  render() {
    return (
      <div>
        <ThemeProvider theme={this.props.themToDoList}>
          <Container className="w-50">
            <Dropdown
              onChange={(e) => {
                let { value } = e.target;
                this.props.dispatch(changeTheme(value));
              }}
            >
              {this.renderTheme()}
            </Dropdown>
            <Heading3>To Do List</Heading3>

            <TextField
              onChange={(e) => {
                this.setState({ taskName: e.target.value });
              }}
              value={this.state.taskName}
              name="taskName"
              label="task name"
            ></TextField>

            <Button
              onClick={() => {
                let { taskName } = this.state;
                let newtask = {
                  id: Date.now(),
                  taskName: taskName,
                  done: false,
                };

                this.props.dispatch(addTaskAction(newtask));
              }}
              className="ml-2 "
            >
              <i className="fa fa-plus"></i> Add task
            </Button>
            {this.state.disabled ? (
              <Button
                disabled
                onClick={() => {
                  this.props.dispatch(updateTask(this.state.taskName));
                }}
                className="ml-2 "
              >
                <i className="fa fa-upload"></i>
                Update task
              </Button>
            ) : (
              <Button
                onClick={() => {
                  let { taskName } = this.state;
                  this.setState({ disabled: true, taskName: "" }, () => {
                    this.props.dispatch(updateTask(taskName));
                  });
                }}
                className="ml-2 "
              >
                <i className="fa fa-upload"></i>
                Update task
              </Button>
            )}

            <hr />
            <Heading3>Task to do</Heading3>
            <Table className="text-center ">
              <Thead>
                <Tr>
                  <Th style={{ verticalAlign: "middle" }}>Task name</Th>
                </Tr>
              </Thead>
              <Tbody>
                {this.props.taskList
                  .filter((task) => !task.done)
                  .map((item, index) => {
                    return (
                      <Tr key={index}>
                        <Th style={{ verticalAlign: "middle" }}>
                          {item.taskName}
                        </Th>
                        <Th className="text-right">
                          <Button
                            onClick={() => {
                              this.setState(
                                {
                                  disabled: false,
                                },
                                () => {
                                  this.props.dispatch(editTask(item));
                                }
                              );
                            }}
                            className="ml-1"
                          >
                            <i className="fa fa-edit"></i>
                          </Button>
                          <Button
                            onClick={() => {
                              this.props.dispatch(doneTask(item.id));
                            }}
                            className="ml-1"
                          >
                            <i className="fa fa-check"></i>
                          </Button>
                          <Button
                            onClick={() => {
                              this.props.dispatch(deleteTask(item.id));
                            }}
                            className="ml-1"
                          >
                            <i className="fa fa-trash"></i>
                          </Button>
                        </Th>
                      </Tr>
                    );
                  })}
              </Tbody>
            </Table>
            <Heading3>Task complete</Heading3>
            <Table className="text-center">
              <Thead>
                <Tr>
                  <Th style={{ verticalAlign: "middle" }}>Task name</Th>
                </Tr>
              </Thead>
              <Tbody>
                {this.props.taskList
                  .filter((task) => task.done)
                  .map((item, index) => {
                    return (
                      <Tr key={index}>
                        <Th>{item.taskName}</Th>
                        <Th className="text-right">
                          <Button
                            onClick={() => {
                              this.props.dispatch(deleteTask(item.id));
                            }}
                            className="ml-1"
                          >
                            <i className="fa fa-trash"></i>
                          </Button>
                        </Th>
                      </Tr>
                    );
                  })}
              </Tbody>
            </Table>
          </Container>
        </ThemeProvider>
      </div>
    );
  }
  //  đây là lifecycle trả về props cũ và state cũ của component trước khi render ( lifecycle này chạy sau khi render)
  componentDidUpdate(prevProps, prevState) {
    //   so sánh nếu như props trước đó ( taskEdit trước mà khác taskEdit hiện tại thì mình mới setState)
    if (prevProps.taskEdit.id !== this.props.taskEdit.id) {
      this.setState({
        taskName: this.props.taskEdit.taskName,
      });
    }
  }
}
let mapStateToProps = (state) => {
  return {
    themToDoList: state.toDoListReducer.themeToDoList,
    taskList: state.toDoListReducer.taskList,
    taskEdit: state.toDoListReducer.taskEdit,
  };
};

export default connect(mapStateToProps)(ToDoList);
