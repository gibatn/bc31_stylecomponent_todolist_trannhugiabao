import React, { Component } from "react";
import ChildComponent from "./ChildComponent";

export default class LifeCycleReact extends Component {
  constructor(props) {
    super(props);
    this.state = {
      number: 1,
    };
  }

  // được gọi khi component này được sử dụng trên DOM (giao diện của  app)
  static getDerivedStateFromProps(newProps, currentState) {
    console.log("getDerivedStateFromProps");
    return null;
  }
  // được gọi khi setstate or props
  shouldComponentUpdate() {
    //     return true : thì chạy tiếp các lifecycle còn lại, return false:ngược lại
    return true;
  }
  render() {
    return (
      <div>
        <h1>Parent component</h1>
        <span>Number:{this.state.number}</span>
        <button
          onClick={() => {
            this.setState({ number: this.state.number + 1 });
          }}
        >
          +
        </button>
        {this.state.number === 1 ? <ChildComponent /> : ""}
      </div>
    );
  }

  //    được gọi sau render và chỉ gọi 1 lần  duy nhất ( trạng thái mounting)
  componentDidMount() {
    console.log("componentDidMount");
  }

  //   lần đầu không gọi chỉ gọi khi setstate or thay đổi props
  componentDidUpdate() {
    console.log("componentDidUpdate");
  }
}
