import React, { Component } from "react";
import styled, { ThemeProvider } from "styled-components";

const configDarkTheme = {
  backgroundColor: "black",
  color: "white",
};
const configLightTheme = {
  backgroundColor: "gray",
  color: "pink",
};

export default class DemoTheme extends Component {
  state = {
    currentTheme: configDarkTheme,
  };
  handleChangeTheme = (event) => {
    this.setState({
      currentTheme:
        event.target.value == "1" ? configDarkTheme : configLightTheme,
    });
  };
  render() {
    const DivStyle = styled.div`
      color: ${(props) => props.theme.color};
      padding: 5%;
      background-color: ${(props) => props.theme.backgroundColor};
      font-size: 2rem;
    `;
    return (
      <ThemeProvider theme={this.state.currentTheme}>
        <DivStyle>Hello mình là yasuo thông thạo 7</DivStyle>
        <select onChange={this.handleChangeTheme}>
          <option value="1">Dark</option>
          <option value="2">Light</option>
        </select>
      </ThemeProvider>
    );
  }
}
