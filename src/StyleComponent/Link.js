import styled from "styled-components";
import React from "react";
//  2 cách như  nhau
// export const Link = (props) => {
//     return <a className={props.className}>{props.children}</a>
// }

export const Link = ({ className, children, ...restProps }) => (
  <a className={className}>{children}</a>
);
export const StyledLink = styled(Link)`
  color: red;
  font-weight: bold;
  font-size: 2rem;
  background-color: blue;
  color: green !important;
  &:hover {
    background-color: pink;
  }
`;
