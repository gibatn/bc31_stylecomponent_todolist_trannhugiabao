import styled from "styled-components";

export const TextFields = styled.input`
  color: ${(props) => props.inputColor || "red"};
`;

export const Thing = styled.div.attrs(() => ({ tabIndex: 0 }))`
  color: blue;
  font-weight: bold;
  font-size: 2rem;
  &:hover {
    color: red;
  }
  & + & {
    background: lime;
  }

  &.something {
    background: orange;
  }
`;
