import styled from "styled-components";
export const Button = styled.button`
  background: ${(props) => (props.bgprimary ? "green" : "red")};
  color: #fff;
  padding: 1rem;
  &:hover {
    opacity: 0.5;
    transition: all 1s;
    color: red;
  }
`;
export const SmallButton = styled(Button)`
  background-color: organe;
  font-size: 0.5rem;
`;
