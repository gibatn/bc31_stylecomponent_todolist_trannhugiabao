import React, { Component } from "react";
import { Button, SmallButton } from "./button";
import { StyledLink } from "./Link";
import { TextFields, Thing } from "./TextFields";
export default class DemoJSS extends Component {
  render() {
    return (
      <div>
        <Button className="button-style" bgprimary>
          BẢO
        </Button>
        <SmallButton>Hello Bảo</SmallButton>

        <StyledLink>ahihih</StyledLink>
        <br />
        <TextFields inputColor="green" />

        {/* <Thing>Hello word</Thing>
        <Thing>Ya hell</Thing>
        <Thing className="something">urge</Thing>

        <Thing>Ba BA</Thing>
        <div>Perrty</div>
        <Thing>Beauty</Thing>
        <Thing>Beauty</Thing>
        <Thing>Beauty</Thing> */}
      </div>
    );
  }
}
