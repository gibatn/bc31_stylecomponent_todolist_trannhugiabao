import { ToDoListDarkTheme } from "../../Theme/ToDoListDarkThem";
import { ToDoListLightTheme } from "../../Theme/ToDoListLightTheme";
import { ToDoListPrimaryTheme } from "../../Theme/ToDoListPrimaryTheme";
import {
  add_task,
  change_theme,
  delete_task,
  done_task,
  edit_task,
  update_task,
} from "../actions/types/ToDoListTypes";
import { arrTheme } from "../../Theme/ThemeManager";
const initialState = {
  themeToDoList: arrTheme[0].theme,
  taskList: [
    { id: 1, taskName: "task1", done: true },
    { id: 2, taskName: "task2", done: false },
    { id: 3, taskName: "task3", done: false },
    { id: 4, taskName: "task4", done: true },
  ],
  taskEdit: { id: -1, taskName: "", done: false },
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case add_task: {
      if (payload.taskName.trim() === "") {
        alert("Task name is required!");
        return { ...state };
      }
      let taskListUpdate = [...state.taskList];
      let index = taskListUpdate.findIndex((item) => {
        return item.taskName == payload.taskName;
      });
      if (index !== -1) {
        alert("task name already exists");
        return { ...state };
      }
      // taskListUpdate.push(payload); c2
      state.taskList = [...taskListUpdate, payload];
      return { ...state };
    }
    case change_theme: {
      let theme = arrTheme.find((theme) => theme.id == payload);
      if (theme) {
        state.themeToDoList = { ...theme.theme };
        return { ...state };
      }
    }
    case done_task: {
      let taskListUpdate = [...state.taskList];
      let index = taskListUpdate.findIndex((item) => item.id == payload);
      if (index !== -1) {
        taskListUpdate[index].done = true;
      }
      return { ...state, taskList: taskListUpdate };
    }
    case delete_task: {
      let taskListUpdate = [...state.taskList];
      // c1
      // let index = taskListUpdate.findIndex((item) => item.id == payload);
      // if (index !== -1) {
      //   taskListUpdate.splice(index, 1);
      // }
      taskListUpdate = taskListUpdate.filter((item) => item.id !== payload);
      return { ...state, taskList: taskListUpdate };
    }
    case edit_task: {
      return { ...state, taskEdit: payload };
    }
    case update_task: {
      //  chỉnh sửa taskname của taskEdit
      state.taskEdit = { ...state.taskEdit, taskName: payload };
      //  tìm trong taskList cập nhật lại taskEdit người dùng update
      let taskListUpdate = [...state.taskList];
      let index = taskListUpdate.findIndex(
        (item) => item.id == state.taskEdit.id
      );
      console.log(index);
      if (index !== -1) {
        taskListUpdate[index] = state.taskEdit;
      }
      state.taskList = taskListUpdate;
      state.taskEdit = { id: -1, taskName: "", done: false };
      return { ...state };
    }

    default:
      return state;
  }
};
