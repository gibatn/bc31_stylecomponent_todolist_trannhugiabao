import {
  add_task,
  change_theme,
  delete_task,
  done_task,
  edit_task,
  update_task,
} from "./types/ToDoListTypes";

export const addTaskAction = (newTask) => ({
  type: add_task,
  payload: newTask,
});

export const changeTheme = (payload) => ({
  type: change_theme,
  payload,
});
export const doneTask = (payload) => ({
  type: done_task,
  payload,
});
export const deleteTask = (payload) => ({
  type: delete_task,
  payload,
});

export const editTask = (payload) => ({
  type: edit_task,
  payload,
});

export const updateTask = (payload) => ({
  type: update_task,
  payload,
});
