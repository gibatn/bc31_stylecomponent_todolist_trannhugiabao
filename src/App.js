import logo from "./logo.svg";
import "./App.css";
import React from "react";
import DemoJSS from "./StyleComponent/DemoJSS";
import DemoTheme from "./StyleComponent/Theme/DemoTheme";
import ToDoList from "./ToDoList/ToDoList";
import LifeCycleReact from "./LifeCycleReact/LifeCycleReact";
function App() {
  return (
    <div className="App">
      {/* <DemoJSS /> */}
      {/* <DemoTheme /> */}
      <ToDoList />
      {/* <LifeCycleReact /> */}
    </div>
  );
}

export default App;
