export const ToDoListDarkTheme = {
  bgcolor: "black",
  color: "white",
  borderButton: "1px solid #fff",
  borderRadiusButton: "none",
  hoverTextColor: "black",
  hoverBgColor: "white",
  boderColor: "black",
};
