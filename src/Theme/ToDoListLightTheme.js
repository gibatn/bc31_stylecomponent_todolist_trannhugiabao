export const ToDoListLightTheme = {
  bgcolor: "white",
  color: "black",
  borderButton: "1px solid #fff",
  borderRadiusButton: "none",
  hoverTextColor: "white",
  hoverBgColor: "black",
  boderColor: "brown",
};
