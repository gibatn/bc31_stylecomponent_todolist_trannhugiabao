export const ToDoListPrimaryTheme = {
  bgcolor: "pink",
  color: "blue",
  borderButton: "1px solid #fff",
  borderRadiusButton: "none",
  hoverTextColor: "gray",
  hoverBgColor: "red",
  boderColor: "yellow",
};
